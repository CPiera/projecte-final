package hello.application.dto;

import com.google.gson.annotations.Expose;

import hello.domain.Picture;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

public class PictureDTO {

	@Expose
	private int id;
	@Expose
	private String description;
	@Expose
	private String location;
	@Expose
	private String urlPicture;
	@Expose
	private BirdDTO birdDTO;

	public PictureDTO(Picture picture) throws NotFoundException, InvalidParamException {
		if (picture == null)
			throw new NotFoundException();
		this.id = picture.getId();
		this.description = picture.getDescription();
		this.location = picture.getLocation();
		this.urlPicture = picture.getUrlPhoto();
		this.birdDTO = new BirdDTO(picture.getBird());
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		if (this.description == null)
			return "";
		return description;
	}

	public String getLocation() {
		if (this.location == null)
			return "";
		return location;
	}

	public String getUrlPhoto() {
		if (this.urlPicture == null)
			return "";
		return urlPicture;
	}

	public BirdDTO getBirdDTO() {
		return birdDTO;
	}

}
