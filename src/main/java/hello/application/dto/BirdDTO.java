package hello.application.dto;

import com.google.gson.annotations.Expose;

import hello.domain.Bird;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

public class BirdDTO {

	@Expose
	private int idBird;
	@Expose
	private String name;
	@Expose
	private int family;
	@Expose
	private String habitat;

	public BirdDTO(Bird bird) throws NotFoundException, InvalidParamException {
		if (bird == null)
			throw new NotFoundException();
		this.idBird = bird.getIdBird();
		this.name = bird.getName();
		this.habitat = bird.getHabitat();
		this.family = bird.getFamily();

	}

	public int getIdBird() {
		return idBird;
	}

	public String getName() {
		if (this.name == null)
			return "";
		return name;
	}

	public int getFamily() {
		return family;
	}

	public String getHabitat() {
		if (this.habitat == null)
			return "";
		return habitat;
	}

}
