package hello.application;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import hello.application.dto.BirdDTO;
import hello.domain.Bird;
import hello.persistance.BirdRepository;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

@Controller
public class BirdController {

	@Autowired
	private BirdRepository birdRepository;

	public BirdDTO registerBird(BirdDTO birdDTO) throws InvalidParamException, NotFoundException {

		Bird bird = new Bird(birdDTO.getName(), birdDTO.getFamily(), birdDTO.getHabitat());

		birdRepository.save(bird);

		return new BirdDTO(bird);
	}

	public Bird getBird(int birdId) throws NotFoundException, InvalidParamException {
		Bird bird = birdRepository.getBirdById(birdId);
		return bird;
	}

	public BirdDTO getBirdDTO(int birdId) throws NotFoundException, InvalidParamException {
		Bird bird = birdRepository.getBirdById(birdId);
		return new BirdDTO(bird);
	}

	public List<BirdDTO> getAllBirds() throws NotFoundException, InvalidParamException {
		List<Bird> birdList = birdRepository.getAllBirds();
		List<BirdDTO> birdDTOList = new ArrayList<>();

		if (birdList.isEmpty())
			throw new NotFoundException();

		for (Bird b : birdList) {
			birdDTOList.add(new BirdDTO(b));
		}

		return birdDTOList;
	}

	public void removeBirdById(int birdId) {
		birdRepository.removeById(birdId);

	}

}
