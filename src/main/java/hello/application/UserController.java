package hello.application;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import hello.application.dto.PictureDTO;
import hello.application.dto.UserDTO;
import hello.domain.Bird;
import hello.domain.Picture;
import hello.domain.User;
import hello.persistance.PictureRepository;
import hello.persistance.UserRepository;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

@Controller
public class UserController {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PictureRepository pictureRepository;

	@Autowired
	private BirdController birdController;

	public UserDTO register(UserDTO userDTO) throws InvalidParamException, NotFoundException {
		userRepository.checkNotRepeatedEmail(userDTO.getEmail());

		User user = new User(userDTO.getName(), userDTO.getEmail(), userDTO.getPassword());
		userRepository.save(user);
		return new UserDTO(user);

	}

	public UserDTO loginUser(UserDTO userToLogin) throws InvalidParamException, NotFoundException {
		User user = userRepository.getUserByEmail(userToLogin.getEmail());
		user.checkPasswordCorrect(userToLogin.getPassword());
		return new UserDTO(user);
	}

	User getUser(int userId) throws NotFoundException {
		return userRepository.getUserById(userId);

	}

	public UserDTO getUserDTO(int userId) throws NotFoundException {
		User userDTO = getUser(userId);
		return new UserDTO(userDTO);
	}

	public UserDTO updateUser(int userId, UserDTO userToUpdate) throws NotFoundException, InvalidParamException {
		User user = userRepository.getUserById(userId);
		user.setName(userToUpdate.getName());
		user.setEmail(userToUpdate.getEmail());
		userRepository.save(user);
		return new UserDTO(user);
	}

	public void removeUserById(int userId) {
		userRepository.removeById(userId);

	}

	public PictureDTO uploadPicture(int userId, PictureDTO pictureToUpload)
			throws InvalidParamException, NotFoundException {
		User user = getUser(userId);
		int birdId = pictureToUpload.getBirdDTO().getIdBird();
		Bird bird = birdController.getBird(birdId);
		Picture picture = new Picture(user, bird, pictureToUpload);
		user.addPictures(picture);
		pictureRepository.save(picture);
		return new PictureDTO(picture);

	}

	public PictureDTO getPicture(int userId, int pictureId) throws NotFoundException, InvalidParamException {
		List<PictureDTO> picturesDTO = getAllPictures(userId);
		PictureDTO result = null;
		for (PictureDTO p : picturesDTO) {
			if (p.getId() == pictureId)
				result = p;
		}
		return result;

	}

	public List<PictureDTO> getAllPictures(int userId) throws NotFoundException, InvalidParamException {
		User user = userRepository.getUserById(userId);
		List<Picture> result = user.getPictures();
		List<PictureDTO> resultDTO = new ArrayList<>();
		for (Picture p : result) {
			resultDTO.add(new PictureDTO(p));
		}
		return resultDTO;
	}

	public void removePictureById(int id) throws NotFoundException, InvalidParamException {
		pictureRepository.removeById(id);

	}

}
