package hello.persistance;

import org.springframework.data.repository.CrudRepository;

import hello.domain.Picture;

public interface HelperPictureRepository extends CrudRepository<Picture, Integer> {
}
