package hello.persistance;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import hello.domain.Bird;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

@Repository
public class BirdRepository {

	@Autowired
	private HelperBirdRepository birdrepository;

	public void save(Bird bird) throws InvalidParamException {
		if (bird == null)
			throw new InvalidParamException();
		try {
			birdrepository.save(bird);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalidParamException();
		}
	}

	public Bird getBirdById(int birdId) throws NotFoundException {
		try {
			return birdrepository.findById(birdId).get();
		} catch (Exception e) {
			throw new NotFoundException();
		}
	}

	public List<Bird> getAllBirds() {
		List<Bird> result = new ArrayList<>();
		for (Bird b : birdrepository.findAll()) {
			result.add(b);
		}
		return result;
	}

	public void checkBirdExist(int idBird) throws NotFoundException {
		List<Bird> birds = getAllBirds();
		for (Bird b : birds) {
			if (b.getIdBird() == idBird)
				return;
		}
		throw new NotFoundException("Ocell inexistent");

	}

	public void removeById(int birdId) {
		birdrepository.deleteById(birdId);

	}

}
