package hello.persistance;

import org.springframework.data.repository.CrudRepository;

import hello.domain.Bird;

public interface HelperBirdRepository extends CrudRepository<Bird, Integer> {

}
