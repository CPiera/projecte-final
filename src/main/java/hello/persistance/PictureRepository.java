package hello.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import hello.domain.Picture;
import hello.utilities.InvalidParamException;

@Repository
public class PictureRepository {

	@Autowired
	private HelperPictureRepository repository;

	public void save(Picture picture) throws InvalidParamException {
		if (picture == null)
			throw new InvalidParamException();
		try {
			repository.save(picture);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalidParamException("no s'ha pujat");
		}
	}

	public void removeById(Integer id) {
		repository.deleteById(id);
	}

}
