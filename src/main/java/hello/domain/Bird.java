package hello.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import hello.utilities.InvalidParamException;

@Entity(name = "bird")
public class Bird {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id_bird")
	private int idBird;
	private String name;
	private int family;
	private String habitat;

	public Bird() {

	}

	public Bird(String name, int family, String habitat) throws InvalidParamException {
		checkAll(name, family, habitat);
		this.name = name;
		this.family = family;
		this.habitat = habitat;

	}

	public void checkAll(String name, int family, String habitat) throws InvalidParamException {
		checkName(name);
		checkFamily(family);
		checkHabitat(habitat);
	}

	public void checkName(String name) throws InvalidParamException {
		if (name.equals(""))
			throw new InvalidParamException();
	}

	public void checkFamily(int family) throws InvalidParamException {
		if (family <= 0)
			throw new InvalidParamException("Familia incorrecte");

	}

	public void checkHabitat(String habitat) throws InvalidParamException {
		if (habitat.equals(""))
			throw new InvalidParamException();
	}

	public int getIdBird() {
		return idBird;
	}

	public String getName() {
		return name;
	}

	public int getFamily() {
		return family;
	}

	public String getHabitat() {
		return habitat;
	}

}
