package hello.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import hello.application.dto.PictureDTO;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

@Entity(name = "picture")
public class Picture {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	private String description;
	private String location;
	@Column(name = "url_picture")
	private String urlPicture;

	@ManyToOne(targetEntity = Bird.class)
	@JoinColumn(name = "id_bird")
	private Bird bird;

	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id")
	private User user;

	public Picture() {

	}

	public Picture(User user, Bird bird, PictureDTO pictureDTO) throws InvalidParamException, NotFoundException {
		if (bird == null)
			throw new NotFoundException();
		checkAll(pictureDTO.getDescription(), pictureDTO.getLocation(), pictureDTO.getUrlPhoto());
		this.description = pictureDTO.getDescription();
		this.location = pictureDTO.getLocation();
		this.urlPicture = pictureDTO.getUrlPhoto();
		this.bird = bird;
		this.user = user;
	}

	public void checkAll(String description, String location, String urlPhoto) throws InvalidParamException {
		checkDescription(description);
		checkLocation(location);
		checkUrlPhoto(urlPhoto);
	}

	public void checkDescription(String description) throws InvalidParamException {
		if (description.equals(""))
			throw new InvalidParamException();
	}

	public void checkLocation(String location) throws InvalidParamException {
		if (location.equals(""))
			throw new InvalidParamException();
	}

	public void checkUrlPhoto(String urlPhoto) throws InvalidParamException {
		if (urlPhoto.contains(".jpg"))
			throw new InvalidParamException();
	}

	public Integer getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public String getLocation() {
		return location;
	}

	public String getUrlPhoto() {
		return urlPicture;
	}

	public Bird getBird() {
		return bird;
	}

	public Integer getBirdId() {
		return bird.getIdBird();
	}

}
