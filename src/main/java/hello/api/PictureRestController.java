package hello.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import hello.application.UserController;
import hello.application.dto.PictureDTO;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

@CrossOrigin
@RestController
@RequestMapping("/users/{userId}/pictures")
public class PictureRestController {

	@Autowired
	private UserController usercontroller;

	@PostMapping
	public String upload(@PathVariable int userId, @RequestBody String jPicture)
			throws InvalidParamException, NotFoundException {
		PictureDTO picture = new Gson().fromJson(jPicture, PictureDTO.class);
		PictureDTO result = usercontroller.uploadPicture(userId, picture);
		return new Gson().toJson(result);
	}

	@GetMapping
	public String getAllPicturesByUser(@PathVariable int userId) throws Exception {
		List<PictureDTO> pictures = usercontroller.getAllPictures(userId);
		return toJson(pictures);
	}

	@GetMapping(value = "/{pictureId}", produces = "application/json;charset=UTF-8")
	public String getPicture(@PathVariable int userId, @PathVariable int pictureId) throws Exception {
		PictureDTO picture = usercontroller.getPicture(userId, pictureId);
		return toJson(picture);
	}

	@DeleteMapping(value = "/{pictureId}", produces = "application/json;charset=UTF-8")
	public void removePictureById(@PathVariable int pictureId) throws Exception {
		usercontroller.removePictureById(pictureId);
	}

	private String toJson(Object object) {

		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(object);
	}

}
