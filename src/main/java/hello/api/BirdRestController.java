package hello.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import hello.application.BirdController;
import hello.application.dto.BirdDTO;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

@CrossOrigin
@RestController

public class BirdRestController {

	@Autowired
	private BirdController birdController;

	@PostMapping(value = "/birds", produces = "application/json;charset=UTF-8")
	public String upload(@RequestBody String jBird) throws NotFoundException, InvalidParamException {

		BirdDTO newBird = new Gson().fromJson(jBird, BirdDTO.class);

		BirdDTO bird = birdController.registerBird(newBird);

		return toJson(bird);
	}

	@GetMapping(value = "/birds/{birdId}", produces = "application/json;charset=UTF-8")
	public String getBird(@PathVariable int birdId) throws Exception {
		BirdDTO bird = birdController.getBirdDTO(birdId);
		return toJson(bird);
	}

	@GetMapping(value = "/birds", produces = "application/json;charset=UTF-8")
	public String listBirds() throws NotFoundException, InvalidParamException {

		List<BirdDTO> birds = birdController.getAllBirds();

		return toJson(birds);
	}

	@DeleteMapping(value = "/birds/{birdId}", produces = "application/json;charset=UTF-8")
	public void removePictureById(@PathVariable int birdId) throws Exception {
		birdController.removeBirdById(birdId);
	}

	private String toJson(Object object) {

		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(object);
	}

}
